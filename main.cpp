#include <QCoreApplication>
#include <iostream>
#include <QDirIterator>
#include <QUrl>
#include <QDebug>
#include <QProcess>
#include <QFile>


static void cleanSvgs(QString url)
{
    QFile::remove(url);
}

static void processUrls(const QUrl &root, const QStringList &filters, void (*cb)(QString url))
{
    QDirIterator it (root.toLocalFile(), {"*.svg"}, QDir::Files | QDir::AllDirs | QDir::NoDotDot | QDir::NoDot, QDirIterator::Subdirectories | QDirIterator::FollowSymlinks);
    while (it.hasNext())
        cb(it.next());
}

static void createPng(QString url)
{
    qDebug() << "PNG FOR" << url;
    const QString where = QString(url).replace(".svg", ".png");
    QFileInfo info(url);

    if(QFileInfo::exists(where))
        return;

    if(info.isSymLink())
        url = info.symLinkTarget() ;

    qDebug()<< QString("inkscape %1 -e %2").arg(url, where);

    QProcess process;
    process.start(QString("inkscape %1 -e %2").arg(url, where));
    process.waitForFinished();
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::string path_;

    std::cout << "Root path: ";
    std::cin >> path_;

    processUrls(QUrl::fromUserInput(QString::fromStdString(path_)), {"*.svg"}, createPng);
    processUrls(QUrl::fromUserInput(QString::fromStdString(path_)), {"*.svg"}, cleanSvgs);

    return a.exec();
}
